import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Categoria from "../views/Categoria.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    redirect: "/categorias/1",
  },
  {
    path: "/categorias",
    name: "categorias",
    redirect: "/",
    component: Home,
    children: [
      {
        path: ":categoria",
        name: "categoria",
        component: Categoria,
        props: true,
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
