export default {
  data() {
    return {
      api: null,
    };
  },
  methods: {
    async fetchData(urlParam = 1, filter = "") {
      const url = await fetch(
        `http://localhost:8888/api/V1/categories/${urlParam}`
      );
      const response = await url.json();
      this.api = response;
      this.$store.state.categoriaAtual = this.api.type[0].name;
      this.$store.state.filtro = [];
      this.$store.state.apiFiltro = [];
      this.dadosFiltro();

      const responseFilter = response.items;
      responseFilter.forEach((item) => {
        const itemFiltro = Object.values(item.filter[0])[0];
        if (itemFiltro.includes(filter)) {
          // console.log(item, itemFiltro, filter);
          this.$store.state.apiFiltro.push(item);
        }
      });
    },
    dadosFiltro() {
      const typeName = Object.values(this.api.filters[0])[0];
      this.$store.state.nomeFiltro = typeName;

      const items = this.api.items;
      items.forEach((item) => {
        const itemFiltro = Object.values(item.filter[0])[0];
        if (!this.$store.state.filtro.includes(itemFiltro)) {
          this.$store.state.filtro.push(itemFiltro);
        }
      });
    },
    sortOrder(order, typeOrder = "preco-asc") {
      const api = this.$store.state.apiFiltro;

      api.sort(
        (a, b) =>
          (a.specialPrice ? a.specialPrice : a.price) -
          (b.specialPrice ? b.specialPrice : b.price)
      );
      if (typeOrder === "preco-desc") api.reverse();
    },
  },
};
