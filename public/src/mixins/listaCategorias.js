export default {
  data() {
    return {
      categoriasFiltro: [],
    };
  },
  methods: {
    async listaCategorias() {
      const url = await fetch("http://localhost:8888/api/V1/categories/list");
      const response = await url.json();
      this.categoriasFiltro = response.items;
    },
  },
};
